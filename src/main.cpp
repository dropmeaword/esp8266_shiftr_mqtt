#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <PubSubClient.h>

#include "config.h"

WiFiClient wifi;
PubSubClient mqtt(wifi);

// the name of this device, we start with "unknown" and we will later generate one
String deviceId = "unknown";

long lastMsg = 0;
char msg[50];
int value = 0;

void setup_wifi() {
  delay(10);

  // we start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(wifi_ssid);

  WiFi.begin(wifi_ssid, wifi_password);

  // keep trying until we are connected
  // @WARNING this will stall if the network is not available and hang here forever
  // @TODO time-out here at some point if we do not succeed at connecting
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
    yield();
  }

  // init our random number genrator
  randomSeed(micros());

  // print connection details when we are finally connected to the configured WiFi
  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void callback_mqtt_received(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
  Serial.println();

  // Switch on the LED if an 1 was received as first character
  if ((char)payload[0] == '1') {
    digitalWrite(BUILTIN_LED, LOW);   // Turn the LED on (Note that LOW is the voltage level
    // but actually the LED is on; this is because
    // it is active low on the ESP-01)
  } else {
    digitalWrite(BUILTIN_LED, HIGH);  // Turn the LED off by making the voltage HIGH
  }

}

/// create a random client ID
String makeDeviceId() {
    String retval;

    uint64_t chipid = ESP.getChipId();

    // this makes a random ID on every run, nice, but we don't really want that
    // retval = String((int)chipid, HEX) + "-" + String(random(0xffff), HEX);

    // so we make an ID that is bound to the device and stays the same on every run
    // and we give it a fixed prefix so we can find all of our devices when we need them
    retval = String("qi-") + String((int)chipid, HEX);

    return retval;
}

void reconnect() {
  // loop until we're reconnected
  while (!mqtt.connected()) {
    Serial.print("Attempting MQTT connection...");

    if (mqtt.connect(deviceId.c_str(), mqtt_broker_token, mqtt_broker_token_key)) {
      Serial.println("connected");
    } else {
      Serial.print("failed, rc=");
      Serial.print(mqtt.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

void setup() {

  pinMode(BUILTIN_LED, OUTPUT);     // initialize the BUILTIN_LED pin as an output
  Serial.begin(115200);

  // make up a name for this device
  deviceId = makeDeviceId();
  Serial.print("This device can be identified by: ");
  Serial.println(deviceId);

  setup_wifi();

  mqtt.setServer(mqtt_broker, mqtt_broker_port);
  //mqtt.setCallback(callback_mqtt_received);
}

void loop() {

  if (!mqtt.connected()) {
    reconnect();
  }

  long now = millis();
  if (now - lastMsg > 2000) {
    lastMsg = now;
    ++value;

    int temp = random(10, 16);
    int humidity = random(89, 97);

    // here's where you define your payload
    // @LANDINGSTRIP
    String payload = "{";
    payload += "\"temperature\":"; payload += temp; payload += ",";
    payload += "\"humidity\":"; payload += humidity;
    payload += "}";

    // send payload
    char attributes[100];
    payload.toCharArray( attributes, 100 );
    mqtt.publish( mqtt_pub_topic, attributes );
    Serial.println( attributes );
  }

  mqtt.loop();
  yield();
}
